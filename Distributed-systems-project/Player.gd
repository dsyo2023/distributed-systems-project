extends KinematicBody2D

signal local_killed(peer_id) # Local player killed a remote player
signal remote_killed(killer_id, killed_id) # We just witnessed a murder

export var speed = 1000
export var id: int

var spin_sections: int = 0 # Rotation steps
var velocity: Vector2 # Used for extrapolation

# Used for interpolation (smoothing)
var interpolation_start: Vector2
var interpolation_target: Vector2
var interpolating: bool = false # True if in progress
var interpolation_weight: float = 0# 0 to 1
var interpolation_speed: float = Global.interpolation_speed



func _ready():
    disable() # Hide
    if get_parent().name == "RemotePlayers":
        #Set the ID label to show id
        $ID.text = String(id)
        $Sprite2D.modulate = Global.get_random_color()
        # Connect the kill signal
        connect("remote_killed", get_node("../../../Voting"), "_on_Player_remote_killed")
    else:
        $ID.text = "You"
    randomize()
    position = Vector2(rand_range(200,800),rand_range(200,400))
    # Wait before showing
    yield(get_tree().create_timer(2.0),"timeout")
    enable()


func _physics_process(delta):
    if get_parent().name == "RemotePlayers":
        if interpolating:
            interpolation_weight += delta * interpolation_speed
            if interpolation_weight >= 1:
                # Stop interpolating
                self.position = interpolation_target
                interpolating = false
            else:
                self.position = interpolation_start.linear_interpolate(
                    interpolation_target, interpolation_weight)
        else:
            self.move_and_slide(velocity * Global.extrapolation_factor)

    # Spin procedure
    if spin_sections:
        spin_procedure(delta)


func move_by_input() -> Vector2:
    if !self.visible:
        return Vector2.ZERO
    var movement = Vector2()
    if Input.is_action_pressed("move_up"):
        movement.y -= 1
    if Input.is_action_pressed("move_down"):
        movement.y += 1
    if Input.is_action_pressed("move_left"):
        movement.x -= 1
    if Input.is_action_pressed("move_right"):
        movement.x += 1
    # Normalize direction vector to prevent faster movement diagonally
    # Apply speed and delta to make velocity constant
    # Return to send the same vector
    return self.move_and_slide(movement.normalized() * speed)


# Trigger the spin procedure
func spin():
    # Check that the player is not currently spinning
    if not spin_sections:
        # Set rotation steps
        spin_sections = 16


func spin_procedure(_delta):
    # Rotate the sprite 22.5 degrees
    $Sprite2D.rotate(PI / 8)

    # Increase the size for half of the spin
    if spin_sections > 8:
        # Scale just the sprite
        $Sprite2D.scale += Vector2(.1, .1)

    # and then reduce size back to normal
    else:
        # Scale just the sprite
        $Sprite2D.scale -= Vector2(.1, .1)

    if spin_sections == 8:
        check_area()

    # Decrement the rotation steps
    spin_sections -= 1


func check_area():
    var bodies = $Area2D.get_overlapping_bodies()
    bodies.erase(self) # Ignore collision with self
    # Check if currently "running" as the local player
    if get_parent().name == "LocalPlayer":
        for body in bodies:
            # Emit signal for each of the remote players in area
            emit_signal("local_killed", int(body.name))
    else:
        for body in bodies:
            #print(self.name, " killed:", body.name)
            var victim: int
            if body.name == "Player":
                victim = get_tree().get_network_unique_id()
            else:
                victim = int(body.name)
            emit_signal("remote_killed", int(self.name), victim)


func kill_and_respawn():
    var alive = self.visible
    if alive:
        disable()
        spawn() # Spawn has a delay

func disable():
    # Hide the player
    # Make player invisble and disable collisions
    self.visible = false
    get_node("CollisionShape2D").disabled = true

func spawn():
    # Show the player
    # Move player to some position
    randomize()
    self.position = Vector2(rand_range(200,800),rand_range(200,400))
    # Wait 5 seconds
    yield(get_tree().create_timer(5.0), "timeout")
    enable()

func enable():
    # Enable collisions and make the player visible
    get_node("CollisionShape2D").disabled = false
    self.visible = true

