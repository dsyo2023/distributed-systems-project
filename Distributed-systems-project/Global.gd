extends Node

# URL for signaling server
var signaling_server_address: String
# Room key for WebRTC data channel
var room_key: String = ""

var extrapolation_factor: float = 1 # 0 to 1

var interpolation_enabled: bool = true
var interpolation_speed: int = 1000

var opponent_color: Color = Color.lightcoral setget set_color, get_random_color

func set_color(new_color :Color):
    opponent_color = new_color

func get_random_color():
    opponent_color.h += 0.5639 # Constant number to keep somewhat synchronised colors
    if opponent_color.h > 1:
        opponent_color.h -= 1
    return opponent_color
