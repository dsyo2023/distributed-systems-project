extends Node

signal kill_confirmed(peer_id)

const VOTE_THRESHOLD = 0.34 # percent

var my_kills_confirms: Dictionary = {}


func _on_Client_peer_joined(id):
    # Add peer to the dictionary
    my_kills_confirms[id] = 0

func _on_Player_remote_killed(killer_id, killed_id):
    rpc_id(killer_id, "count_vote", killed_id)

func _on_Player_local_killed(peer_id:int):
    my_kills_confirms[peer_id] = 0 # We dont vote

remote func count_vote(killed_peer):
    my_kills_confirms[killed_peer] += 1 # Count the vote
    var total_peers: int = get_tree().get_network_connected_peers().size() + 1
    if my_kills_confirms[killed_peer] == ceil(total_peers * VOTE_THRESHOLD):
        # Vote succeed (VOTE_THRESHOLD% of other players have voted yes)
        emit_signal("kill_confirmed", killed_peer)
