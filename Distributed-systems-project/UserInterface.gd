extends Control

onready var RPCTimer: Timer = get_node("../Players/LocalPlayer/RPCTimer")
onready var PacketRate: Control = $Sliders/PacketRate
onready var Extrapolation: Control = $Sliders/Extrapolation
onready var Interpolation: Control = $Sliders/Interpolation

var player_box_scene = preload("res://player_box.tscn")

var players: Array
var kills: Dictionary = {}
var average_pings: Dictionary = {}

const SELF_REFERENCE := "You"

func _ready():
    PacketRate.get_node("Slider").value = 1 / RPCTimer.wait_time # Get default time from the timer

    _on_PacketRate_value_changed(PacketRate.get_node("Slider").value) # Initialize to default value

    _on_Extrapolation_value_changed(100) #Initialize always to 100, done here to use the same formatting

    _on_Interpolation_value_changed(100)


func _on_PacketRate_value_changed(value):
    #Update value on UI,
    #pad with zeroes, 2 decimals, Packets per second
    PacketRate.get_node("Value").text = "%05.2f PPS" %value

    #Update value on the timer
    RPCTimer.wait_time = 1 / value

func _on_Extrapolation_value_changed(value):
    if value != 0:
        Extrapolation.get_node("Value").text = str(value) + "%" # Value is in percent format
    else:
        Extrapolation.get_node("Value").text = "Disabled"
    Global.extrapolation_factor = value / 100

func _on_Interpolation_value_changed(value):

    if value != 0:
        Interpolation.get_node("Value").text = str(value) + "%"
        Global.interpolation_enabled = true
        Global.interpolation_speed = value * 10
    else:
        Interpolation.get_node("Value").text = "Disabled"
        Global.interpolation_enabled = false


func _input(event):
    if event is InputEventKey:
        if event.is_action_pressed("hide_ui"):
            # Toggle UI visibility
            self.visible = !self.visible
        if event.is_action_pressed("show_score_board"):
            $ScoreBoardPanel.visible = !$ScoreBoardPanel.visible


func _on_Client_joined_room(room_key):
    # Set room key on the UI
    $TopBar/RoomKey.text = "Room: " + room_key + " (copied to clipboard)"

    # Initialize score board with self
    players.push_front(SELF_REFERENCE)
    kills[SELF_REFERENCE] = 0
    average_pings[SELF_REFERENCE] = 0
    _generate_score_board()


func _on_RemotePlayers_tally_kill(peer_id):
    if peer_id == get_tree().get_network_unique_id():
        kills[SELF_REFERENCE] += 1
    else:
        kills[peer_id] += 1

    _generate_score_board()


func _on_Client_peer_joined(id):
    # Add peer to list of peers
    players.append(id)
    for player in players:
        print("test")
        kills[player] = 0
    _generate_score_board()

func _on_Client_peer_disconnected(id):
    players.erase(id)
    kills.erase(id)
    _generate_score_board()


func _on_RTT_rtt_updated(measurements: Dictionary):
    for peer in measurements:
        var list: Array = measurements[peer]
        var sum = 0.0
        if not list.empty():
            for value in list:
                sum += value
            var average = round(sum / list.size())
            average_pings[peer] = average

    _generate_score_board()


func _generate_score_board():
    # TODO Should have its own update script but for now just regenerate it each time
    var children = $ScoreBoardPanel/ScoreBoardBox.get_children()
    for child in children:
        child.queue_free()

    var score_header = player_box_scene.instance()
    $ScoreBoardPanel/ScoreBoardBox.add_child(score_header)

    for player in players:
        var player_box = player_box_scene.instance()
        player_box.get_node("PlayerId").text = str(player)
        player_box.get_node("PlayerKills").text = str(kills[player])
        player_box.get_node("PlayerPing").text = str(average_pings.get(player))

        $ScoreBoardPanel/ScoreBoardBox.add_child(player_box)
