extends Node

signal player_movement(x_position, y_position, x_velocity, y_velocity)
signal player_spin()

onready var local_player: KinematicBody2D = get_child(0)

var velocity: Vector2 = Vector2.ZERO

#var last_velocity: Vector2
#var last_position: Vector2


#func _ready():


func _process(_delta):
    if Input.is_action_just_pressed("action_spin"):
        # Call spin for local player
        local_player.spin()
        # Send signal to RemotePlyers for triggering RPC
        emit_signal("player_spin")

func _physics_process(_delta):
    # Update movement velocity
    velocity = local_player.move_by_input()


# Repeatingly call the rpc signal on "RemotePlayers"
func _on_RPCTimer_timeout():
    var new_position: Vector2 = $Player.position
    emit_signal("player_movement", new_position.x, new_position.y, velocity.x, velocity.y)
