extends Node

signal tally_kill(peer_id) # Signal to increment score board by 1 for specified peer

var player = preload("res://Player.tscn")
onready var update_interval = get_node("../LocalPlayer/RPCTimer").wait_time

#func _ready():
#    pass


#func _process(delta):
#    pass



remote func update_player_position(x_position, y_position, x_velocity, y_velocity):
    var remote_id = get_tree().get_rpc_sender_id()
    var remote_player :KinematicBody2D = get_node(str(remote_id))

    var current_position: Vector2 = remote_player.position
    var target_position: Vector2 = Vector2(x_position, y_position)
    var interpolation_distance: float = current_position.distance_to(target_position)
    if Global.interpolation_enabled and interpolation_distance > 0:
        # Move smoothly to the correct position (adds some delay)
        # Save the current position to interpolate correctly
        remote_player.interpolation_start = remote_player.position
        remote_player.interpolation_target = target_position
        remote_player.interpolation_weight = 0 #Reset interpolation
        # Calculate interpolation speed to be constant
        remote_player.interpolation_speed = Global.interpolation_speed / interpolation_distance
        remote_player.interpolating = true # Start the interpolation, which is handled in the player
    else:
        # Snap to the correct position
        remote_player.position = target_position

    remote_player.velocity = Vector2(x_velocity, y_velocity)


# Sends the local players position, later possibly speed too
func _on_LocalPlayer_player_movement(x_position, y_position, x_velocity, y_velocity):
    rpc_unreliable("update_player_position", x_position, y_position, x_velocity, y_velocity)


remote func spin_player():
    var id = get_tree().get_rpc_sender_id()
    # Call spin on the specified remote player
    get_node(str(id)).spin()


# Sends spin event of local player
func _on_LocalPlayer_player_spin():
    rpc("spin_player")

func _on_Client_peer_joined(id):
    var remote_player = player.instance()
    remote_player.name = str(id)
    remote_player.id = id
    add_child(remote_player)

func _on_Client_peer_disconnected(id):
    var peer :KinematicBody2D = get_node(str(id))
    peer.queue_free() # Remove the peer player


func _on_kill_confirmed(peer_id):
    # Local player, indeed, has killed a peer
    rpc("player_killed", peer_id)

remotesync func player_killed(peer_id):
    # Send signal to increment score board by 1 for specified peer
    # TODO fix double increment
    # TODO fix local player reference
    emit_signal("tally_kill", get_tree().get_rpc_sender_id())
    # See if killed was remote
    var remote_player = get_node_or_null(str(peer_id))
    if remote_player:
        # Someone got rekt
        remote_player.kill_and_respawn()
    else:
        # We just got rekt
        get_node("../LocalPlayer/Player").kill_and_respawn()
