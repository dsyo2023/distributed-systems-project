extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var ui_items = $VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass


func _on_CreateRoom_pressed():
    var url = ui_items.get_node("SignalingServer/Address").text
    var port = ui_items.get_node("SignalingServer/Port").text

    var separator = ":"
    if not port:
        separator = ""
    var address: String = url + separator + port # Combine address and port

    print(address)
    Global.signaling_server_address = address
    get_tree().change_scene("res://main.tscn")


func _on_JoinRoom_pressed():
    var room_key = ui_items.get_node("RoomKey").text

    if not room_key:
        room_key = OS.clipboard

    var url = ui_items.get_node("SignalingServer/Address").text
    var port = ui_items.get_node("SignalingServer/Port").text

    var separator = ":"
    if not port:
        separator = ""
    var address: String = url + separator + port # Combine address and port

    print(address)
    print(room_key)

    Global.signaling_server_address = address
    Global.room_key = room_key
    get_tree().change_scene("res://main.tscn")
