extends Node

signal rtt_updated(measurements)

const RTT_TIMEOUT = 1.0
const RTT_INTERVAL = 5.0

var peer_rtt_measurements: Dictionary = {} # measurements are saved here
var last_sent_ping: int = 0 # Same timestamp used for all peers


# Called when the node enters the scene tree for the first time.
func _ready():
    $RTTTimer.wait_time = RTT_INTERVAL


func _on_Client_peer_joined(id):
    # Initialize peer's rtt measurement array
    peer_rtt_measurements[id] = []
    $RTTTimer.start()

func _on_Client_peer_disconnected(id):
    # Remove peer from scoreboard
    peer_rtt_measurements.erase(id) # Should be safe even if doesn't exist


remote func ping():
    rpc_unreliable_id(get_tree().get_rpc_sender_id(), "pong")

func _on_RTTTimer_timeout():
    # Set current time as the timestamp
    last_sent_ping = Time.get_ticks_msec()
    # Send ping
    rpc_unreliable("ping")

remote func pong():
    var sender = get_tree().get_rpc_sender_id()
    var rtt = Time.get_ticks_msec() - last_sent_ping
    peer_rtt_measurements[sender].append(rtt)
    if peer_rtt_measurements[sender].size() >= 5:
        peer_rtt_measurements[sender].pop_front()

    # Send rtt measurements to scoreboard
    emit_signal("rtt_updated", peer_rtt_measurements)

