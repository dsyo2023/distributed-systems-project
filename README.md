# Distributed systems project: Godot WebRTC P2P multiplayer game

Simple demonstration of using WebRTC data channels for multiplayer game made in [Godot](https://godotengine.org/). Both the game client and the signaling server use Godot. We used [Godot WebRTC signaling demo project](https://github.com/godotengine/godot-demo-projects/tree/master/networking/webrtc_signaling) made by Godot developers as base for the signaling server and client. For non-HTML5 builds we also had to use [GDNative WebRTC plugin](https://github.com/godotengine/webrtc-native).

We used the older standard version 3.5.1 which you can find here: https://godotengine.org/download/3.x/

There is also version that uses .NET/C# for scripting, but the standard version uses Godot's own scripting language called [GDScript](https://docs.godotengine.org/en/3.5/tutorials/scripting/gdscript/).

**Note:** you only need to download the engine/editor if you want to build the project yourself since we provide [pre-built packages](https://gitlab.com/akcel/distributed-systems-project/-/releases/).

## Setup

Download the appropriate versions of both the server and the client packages for your platform from [Releases](https://gitlab.com/akcel/distributed-systems-project/-/releases/).

### Signaling server

After extracting the package contents you can start the server using one of the following commands where the `port` parameter can be any free port you want to use:

**Windows**
```
.\signaling-server.exe --no-window --port=9080
```
**Linux**
```
./signaling-server.x86_64 --port=9080
```

### Client
After extracting the package contents you can just run the exe/binary.

In the view that opens you will have to put the address and port of the signaling server you want to use in the two fields at the top. By default the fields have the address of the signaling server we have hosted. If it is online, feel free to use it if you want.

Then there are two buttons. By pressing the "Create room" button you can start a new game. The room key is automatically copied to your clipboard. You can then give it to someone else so they can join your game. Or you can launch a new instance of the game and use it there if you are just testing by yourself.

You can paste or write a room key in the designated field and then press the "Join room" button to join an on-going game. You can also quickly join a room by just pressing the button while you have a room key on your clipboard. The room key is automatically pasted to the client.


## Gameplay

The actual gameplay is rather simple.
There are textual instructions on controls.

The main excitement comes from tweaking the three sliders which control:

1. Player location sending rate in PPS (Packets per second)

1. Extrapolation factor, predict where the player will keep going
<br>(turn all the way down to disable)

1. Interpolation speed, smoothens abrupt location changes especially with low packet rates
<br>(turn all the way down to disable)

The default settings are good for actually playing the game.

Increasing the location rate improves synchronization with the expense of extensive network load.
Too low will make the game unplayable.

Extrapolation works nicely when combined with interpolation.
Too much will cause it to "overshoot" and then the corrections are more noticeable.
With none at all the players seem like moving between points.

Interpolation smoothens the change between location updates.
Although, it directly increases delay,
small amounts can still improve the gameplay experience considerably.
which is why it was implemented.

With both extrapolation and interpolation disabled the player just teleports around.
Of course higher packet rates can mitigate this but the movement still wouldn't be smooth.
