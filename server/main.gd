extends Node


var port: int
var running: bool = false

func _ready():
    for argument in OS.get_cmdline_args():
        # Find port argument
        if argument.begins_with("--port"):
            # Set port value
            port = int(argument.split("=")[1])

    if not port and OS.has_feature("Server"):
        print("No port given. Use --port=<port_number> parameter")
        get_tree().quit()


func _process(delta):
    # Start server listing on the defined port
    if not running:
        $Server.listen(port)
        running = true


