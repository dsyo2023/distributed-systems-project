extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
    pass # Replace with function body.


func _on_listen_toggled(button_pressed):
    if button_pressed:
        get_node("../Server").listen(int($VBoxContainer/Port.value))
    else:
        get_node("../Server").stop()
